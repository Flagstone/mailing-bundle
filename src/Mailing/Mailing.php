<?php

namespace Flagstone\MailingBundle\Mailing;

use Flagstone\MailingBundle\Mailing\Entity\MailingAddress;
use Flagstone\MailingBundle\Mailing\Entity\MailingAttachment;
use Flagstone\MailingBundle\Mailing\Entity\MailingTemplate;
use Flagstone\MailingBundle\Mailing\Exception\SubjectCannotBeNullException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;

class Mailing
{
    private Mailer              $mailer;
    private MailingAddress      $sender;
    private MailingAddress      $from;
    private ?MailingAddress     $to;
    private MailingAddress      $replyTo;
    private array               $cc = [];
    private array               $bcc = [];
    private ?string             $subject = null;
    private MailingTemplate     $template;
    private array               $attachments = [];

    public function __construct(ParameterBagInterface $parameterBag, MailerInterface $mailer)
    {
        $senderName = $parameterBag->get('default_sender.name');
        $senderEmail = $parameterBag->get('default_sender.email');
        $fromName = $parameterBag->get('default_from.name');
        $fromEmail = $parameterBag->get('default_from.email');
        $replyToName = $parameterBag->get('default_reply_to.name');
        $replyToEmail = $parameterBag->get('default_reply_to.email');
        $this->mailer = $mailer;

        $this->sender = (new MailingAddress)
            ->setName($senderName)
            ->setAddress($senderEmail);
        $this->from = (new MailingAddress)
            ->setName($fromName)
            ->setAddress($fromEmail);
        $this->replyTo = (new MailingAddress)
            ->setName($replyToName)
            ->setAddress($replyToEmail);
        $this->to = null;
    }

    public function getSender(): MailingAddress
    {
        return $this->sender;
    }

    public function getFrom(): MailingAddress
    {
        return $this->from;
    }

    public function getReplyTo(): MailingAddress
    {
        return $this->replyTo;
    }

    public function getTo(): ?MailingAddress
    {
        return $this->to;
    }

    public function getCc(): array
    {
        return $this->cc;
    }

    public function getBcc(): array
    {
        return $this->bcc;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function getTemplate(): MailingTemplate
    {
        return $this->template;
    }

    public function getAttachments(): ?array
    {
        return $this->attachments;
    }

    public function setSender(MailingAddress $sender): Mailing
    {
        $this->sender = $sender;
        return $this;
    }

    public function setFrom(MailingAddress $from): Mailing
    {
        $this->from = $from;
        return $this;
    }

    public function setReplyTo(MailingAddress $replyTo): Mailing
    {
        $this->replyTo = $replyTo;
        return $this;
    }

    public function setTo(MailingAddress $to): Mailing
    {
        $this->to = $to;
        return $this;
    }

    public function addCc(MailingAddress $cc): Mailing
    {
        $this->cc[] = $cc;
        return $this;
    }

    public function addBcc(MailingAddress $bcc): Mailing
    {
        $this->bcc[] = $bcc;
        return $this;
    }

    public function addAttachment(MailingAttachment $attachment): Mailing
    {
        $this->attachments[] = $attachment;
        return $this;
    }

    /**
     * @param   string|null $subject
     * @return  $this
     * @throws  SubjectCannotBeNullException
     */
    public function setSubject(?string $subject): Mailing
    {
        if (empty($subject)) {
            throw new SubjectCannotBeNullException('Subject cannot be null or empty');
        }
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param   MailingTemplate $template
     * @return  $this
     */
    public function setTemplate(MailingTemplate $template): Mailing
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @throws Exception\IncorrectEmailFormatException
     */
    public function send(): void
    {
        $email = (new MailingBuilder($this))->getEmail();

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
        }
    }

}