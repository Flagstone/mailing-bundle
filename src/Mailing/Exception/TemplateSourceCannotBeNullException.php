<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class TemplateSourceCannotBeNullException extends Exception
{

}