<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class MailingSenderAddressNotExistsException extends Exception
{

}