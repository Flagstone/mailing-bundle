<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class MailingToAddressNotExistsException extends Exception
{

}