<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class SubjectCannotBeNullException extends Exception
{

}