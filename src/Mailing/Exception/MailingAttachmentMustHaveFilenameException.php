<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class MailingAttachmentMustHaveFilenameException extends Exception
{

}