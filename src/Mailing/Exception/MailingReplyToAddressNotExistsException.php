<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class MailingReplyToAddressNotExistsException extends Exception
{

}