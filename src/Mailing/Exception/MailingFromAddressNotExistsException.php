<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class MailingFromAddressNotExistsException extends Exception
{

}