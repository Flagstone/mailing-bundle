<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class MailingBccAddressNotExistsException extends Exception
{

}