<?php

namespace Flagstone\MailingBundle\Mailing\Exception;

use Exception;

class IncorrectEmailFormatException extends Exception
{

}