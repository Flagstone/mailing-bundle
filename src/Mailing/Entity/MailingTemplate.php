<?php

namespace Flagstone\MailingBundle\Mailing\Entity;

use Flagstone\MailingBundle\Mailing\Exception\TemplateSourceCannotBeNullException;

class MailingTemplate
{
    private string $templateSource;
    private ?array $context = [];

    public function getTemplateSource(): string
    {
        return $this->templateSource;
    }

    public function getContext(): ?array
    {
        return $this->context;
    }

    /**
     * @param   string|null $templateSource
     * @return  $this
     */
    public function setTemplateSource(?string $templateSource): MailingTemplate
    {
        $this->templateSource = $templateSource;
        return $this;
    }

    public function setContext(? array $context): MailingTemplate
    {
        $this->context = $context;
        return $this;
    }
}