<?php

namespace Flagstone\MailingBundle\Mailing\Entity;

class MailingAttachment
{
    private string $filename;
    private ?string $customName = null;
    private ?string $mimeType = null;

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getCustomName(): ?string
    {
        return $this->customName;
    }

    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    public function setFilename(string $filename): MailingAttachment
    {
        $this->filename = $filename;
        return $this;
    }

    public function setCustomName(?string $customName): MailingAttachment
    {
        $this->customName = $customName;
        return $this;
    }

    public function setMimeType(?string $mimeType): MailingAttachment
    {
        $this->mimeType = $mimeType;
        return $this;
    }
}