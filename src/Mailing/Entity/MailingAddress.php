<?php

namespace Flagstone\MailingBundle\Mailing\Entity;

class MailingAddress
{
    private string $address;

    private ?string $name;

    /**
     * @return  string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return  string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param   string      $address
     * @return  MailingAddress
     */
    public function setAddress(string $address): MailingAddress
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @param   string|null $name
     * @return  MailingAddress
     */
    public function setName(?string $name): MailingAddress
    {
        $this->name = $name;
        return $this;
    }
}