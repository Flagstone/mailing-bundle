<?php

namespace Flagstone\MailingBundle\Mailing;

use Flagstone\MailingBundle\Mailing\Entity\MailingAddress;
use Flagstone\MailingBundle\Mailing\Entity\MailingAttachment;
use Flagstone\MailingBundle\Mailing\Exception\IncorrectEmailFormatException;
use Flagstone\MailingBundle\Mailing\Exception\MailingAttachmentMustHaveFilenameException;
use Flagstone\MailingBundle\Mailing\Exception\MailingBccAddressNotExistsException;
use Flagstone\MailingBundle\Mailing\Exception\MailingCcAddressNotExistsException;
use Flagstone\MailingBundle\Mailing\Exception\MailingFromAddressNotExistsException;
use Flagstone\MailingBundle\Mailing\Exception\MailingReplyToAddressNotExistsException;
use Flagstone\MailingBundle\Mailing\Exception\MailingSenderAddressNotExistsException;
use Flagstone\MailingBundle\Mailing\Exception\MailingToAddressNotExistsException;
use Flagstone\MailingBundle\Mailing\Exception\TemplateSourceCannotBeNullException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;

class MailingBuilder
{
    private TemplatedEmail $email;
    private Mailing $mailing;

    public function __construct(Mailing $mailing)
    {
        $this->mailing = $mailing;
        $this->email = new TemplatedEmail();
    }

    private function build(): void
    {

    }

    /**
     * @return void
     * @throws MailingSenderAddressNotExistsException
     */
    private function buildSender(): void
    {
        if (null === $this->mailing->getSender()->getAddress()) {
            throw new MailingSenderAddressNotExistsException('Sender Email Address must be set');
        }

        if (null === $this->mailing->getFrom()->getName()) {
            $this->email->sender(new Address($this->mailing->getSender()->getAddress()));
        } else {
            $this->email->sender(new Address($this->mailing->getSender()->getAddress(), $this->mailing->getSender()->getName()));
        }
    }

    /**
     * @return void
     * @throws MailingFromAddressNotExistsException
     */
    private function buildFrom(): void
    {
        if (null === $this->mailing->getFrom()->getAddress()) {
            throw new MailingFromAddressNotExistsException('From Email Address must be set');
        }

        if (null === $this->mailing->getFrom()->getName()) {
            $this->email->from(new Address($this->mailing->getFrom()->getAddress()));
        } else {
            $this->email->from(new Address($this->mailing->getFrom()->getAddress(), $this->mailing->getFrom()->getName()));
        }
   }

    /**
     * @return void
     * @throws MailingReplyToAddressNotExistsException
     */
    private function buildReplyTo(): void
    {
        if (null === $this->mailing->getReplyTo()->getAddress()) {
            throw new MailingReplyToAddressNotExistsException('From Email Address must be set');
        }

        if (null === $this->mailing->getFrom()->getName()) {
            $this->email->replyTo(new Address($this->mailing->getFrom()->getAddress()));
        } else {
            $this->email->replyTo(new Address($this->mailing->getFrom()->getAddress(), $this->mailing->getFrom()->getName()));
        }
    }

    /**
     * @return void
     * @throws MailingToAddressNotExistsException
     */
    private function buildTo(): void
    {
        if (null === $this->mailing->getTo()) {
            throw new MailingToAddressNotExistsException('To Address must be set');
        }

        if (null === $this->mailing->getTo()->getAddress()) {
            throw new MailingToAddressNotExistsException('To Email Address must be set');
        }

        if (null === $this->mailing->getTo()->getName()) {
            $this->email->to(new Address($this->mailing->getTo()->getAddress()));
        } else {
            $this->email->to(new Address($this->mailing->getTo()->getAddress(), $this->mailing->getTo()->getName()));
        }
    }

    /**
     * @return void
     * @throws MailingCcAddressNotExistsException
     */
    private function buildCc(): void
    {
        /** @var MailingAddress $cc */
        foreach($this->mailing->getCc() as $cc) {
            if (null === $cc->getAddress()) {
                throw new MailingCcAddressNotExistsException('Cc Email Address must be set');
            }
            if (null === $cc->getName()) {
                $this->email->addCc(new Address($cc->getAddress()));
            } else {
                $this->email->addCc(new Address($cc->getAddress(), $cc->getName()));
            }
        }
    }

    /**
     * @return void
     * @throws MailingBccAddressNotExistsException
     */
    private function buildBcc(): void
    {
        /** @var MailingAddress $bcc */
        foreach($this->mailing->getBcc() as $bcc) {
            if (null === $bcc->getAddress()) {
                throw new MailingBccAddressNotExistsException('Bcc Email Address must be set');
            }
            if (null === $bcc->getName()) {
                $this->email->addBcc(new Address($bcc->getAddress()));
            } else {
                $this->email->addBcc(new Address($bcc->getAddress(), $bcc->getName()));
            }
        }
    }

    /**
     * @return void
     * @throws TemplateSourceCannotBeNullException
     */
    private function buildTemplate(): void
    {
        if (empty($this->mailing->getTemplate()->getTemplateSource())) {
            throw new TemplateSourceCannotBeNullException('The template source cannot be null.');
        }

        $this->email->htmlTemplate($this->mailing->getTemplate()->getTemplateSource());
        $this->email->context($this->mailing->getTemplate()->getContext());
    }

    /**
     * @return void
     * @throws MailingAttachmentMustHaveFilenameException
     */
    private function buildAttachments(): void
    {
        /** @var MailingAttachment $attachment */
        foreach($this->mailing->getAttachments() as $attachment) {
            if (null === $attachment->getFilename()) {
                throw new MailingAttachmentMustHaveFilenameException('Filename of the attachement must be set.');
            }
            if (null === $attachment->getCustomName() && null === $attachment->getMimeType()) {
                $this->email->attachFromPath($attachment->getFilename());
            } elseif (null === $attachment->getMimeType()) {
                $this->email->attachFromPath($attachment->getFilename(), $attachment->getCustomName());
            } else {
                $this->email->attachFromPath($attachment->getFilename(), $attachment->getCustomName(), $attachment->getMimeType());
            }
        }
    }

    /**
     * @return TemplatedEmail
     * @throws IncorrectEmailFormatException
     */
    public function getEmail(): TemplatedEmail
    {
        try {
            $this->buildSender();
        }  catch (MailingSenderAddressNotExistsException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        try {
            $this->buildFrom();
        }  catch (MailingFromAddressNotExistsException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        try {
            $this->buildTo();
        }  catch (MailingToAddressNotExistsException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        try {
            $this->buildCc();
        }  catch (MailingCcAddressNotExistsException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        try {
            $this->buildBcc();
        }  catch (MailingBccAddressNotExistsException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        if (null === $this->mailing->getSubject()) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. Subject cannot be null or empty');
        } else {
            $this->email->subject($this->mailing->getSubject());
        }

        try {
            $this->buildTemplate();
        }  catch (TemplateSourceCannotBeNullException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        try {
            $this->buildAttachments();
        } catch (MailingAttachmentMustHaveFilenameException $e) {
            throw new IncorrectEmailFormatException('Incorrect Email Format. ' . $e->getMessage());
        }

        return $this->email;
    }
}