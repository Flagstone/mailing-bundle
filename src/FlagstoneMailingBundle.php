<?php declare(strict_types=1);
/**
 *  FlagstoneMailingBundle.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/05/02
 */

namespace Flagstone\MailingBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 *  Class FlagstoneMailingBundle
 *
 *  Base class of the Flagstone Mailing project
 *
 *  @package Flagstone\MailingBundle
 */
class FlagstoneMailingBundle extends Bundle
{
    const DOMAIN_TRANS = 'MailingBundle';

    public static function getContext(): string
    {
        return 'flagstone_mailing';
    }
}