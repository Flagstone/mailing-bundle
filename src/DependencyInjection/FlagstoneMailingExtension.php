<?php declare(strict_types=1);
/**
 *  FlagstoneMailingExtension.php
 *
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *
 *  Created: 2022/05/03
 */

namespace Flagstone\MailingBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 *  Class FlagstoneFlashMessageExtension
 *
 *  Resources loader class
 *
 *  @package Flagstone\MailingBundle\DependencyInjection
 */
class FlagstoneMailingExtension extends Extension
{
    /**
     * @param   array               $configs
     * @param   ContainerBuilder    $container
     * @throws  Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}